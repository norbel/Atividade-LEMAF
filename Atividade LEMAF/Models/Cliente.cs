﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class Cliente
    {
        public string TipoCliente { get; private set; }
        public int QuantidadePassageiros { get; private set; }
        public string MenorData { get; private set; }
        public string MaiorData { get; private set; }
        public string TipoCarro { get; private set; }
        public string MelhorEmpresa { get; private set; }
        public float Valor { get; private set; }

        public Cliente(string tipoCliente, int quantidadePassageiros, string menorData, string maiorData, string tipoCarro, string melhorEmpresa, float valor)
        {
            TipoCliente = tipoCliente ?? throw new ArgumentNullException(nameof(tipoCliente));
            QuantidadePassageiros = quantidadePassageiros;
            MenorData = menorData ?? throw new ArgumentNullException(nameof(menorData));
            MaiorData = maiorData ?? throw new ArgumentNullException(nameof(maiorData));
            TipoCarro = tipoCarro ?? throw new ArgumentNullException(nameof(tipoCarro));
            MelhorEmpresa = melhorEmpresa ?? throw new ArgumentNullException(nameof(melhorEmpresa));
            Valor = valor;
        }
    }
}
