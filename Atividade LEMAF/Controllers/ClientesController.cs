using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Models;
using System.Globalization;

namespace Atividade_LEMAF.Controllers
{
    public class ClientesController : Controller
    {
        public List<Cliente> TodosClientes = new List<Cliente>();

        // GET: Clientes
        [HttpPost]
        public ActionResult Index(IFormFile file)
        {
            try
            {
                StreamReader lerArquivo = new StreamReader(file.OpenReadStream());
                while (!lerArquivo.EndOfStream)
                {
                    List<DateTime> listaDatas = new List<DateTime>();
                    var maiorData = DateTime.MinValue;
                    var menorData = DateTime.MaxValue;
                    int diasSemana = 0;
                    int diasFimSemana = 0;
                    string tipoCarro = "";
                    string melhorEmpresa = "";
                    float valor = 0;

                    var linha = lerArquivo.ReadLine();
                    var valores = linha.Split(':');

                    string tipoCliente = valores[0];
                    int numPassageiros = int.Parse(valores[1]);
                    string datas = valores[2];

                    var vetorDatas = datas.Split(',');
                    foreach (string item in vetorDatas)
                    {
                        string aux = item.Trim();
                        listaDatas.Add(DateTime.ParseExact(aux, "ddMMMyyyy(ddd)", CultureInfo.InvariantCulture));
                    }

                    foreach (DateTime data in listaDatas)
                    {
                        if (maiorData < data) maiorData = data;
                        if (menorData > data) menorData = data;
                        if (data.DayOfWeek == DayOfWeek.Saturday || data.DayOfWeek == DayOfWeek.Sunday)
                        {
                            diasFimSemana++;
                        }
                        else
                        {
                            diasSemana++;
                        }
                    }

                    if (numPassageiros > 7)
                    {
                        tipoCarro = "Excesso de";
                        melhorEmpresa = "passageiros";
                        valor = '-';
                    }
                    else if (numPassageiros > 4)
                    {
                        tipoCarro = "SUV�s";
                        melhorEmpresa = "NorthCar";
                        if (tipoCliente == "Normal")
                        {
                            valor = 630 * diasSemana + 600 * diasFimSemana;
                        }
                        else
                        {
                            valor = 580 * diasSemana + 590 * diasFimSemana;
                        }
                    }
                    else if (numPassageiros > 2)
                    {
                        int valor1, valor2;
                        if (tipoCliente == "Normal")
                        {
                            valor1 = 630 * diasSemana + 600 * diasFimSemana;
                            valor2 = 210 * diasSemana + 200 * diasFimSemana;
                        }
                        else
                        {
                            valor1 = 580 * diasSemana + 590 * diasFimSemana;
                            valor2 = 150 * diasSemana + 90 * diasFimSemana;
                        }

                        if (valor1 > valor2)
                        {
                            tipoCarro = "Compactos";
                            melhorEmpresa = "SouthCar";
                            valor = valor2;
                        }
                        else
                        {
                            tipoCarro = "SUV�s";
                            melhorEmpresa = "NorthCar";
                            valor = valor1;
                        }
                    }
                    else if (numPassageiros > 0)
                    {
                        int valor1, valor2, valor3;
                        if (tipoCliente == "Normal")
                        {
                            valor1 = 630 * diasSemana + 600 * diasFimSemana;
                            valor2 = 210 * diasSemana + 200 * diasFimSemana;
                            valor3 = 530 * diasSemana + 200 * diasFimSemana;
                        }
                        else
                        {
                            valor1 = 580 * diasSemana + 590 * diasFimSemana;
                            valor2 = 150 * diasSemana + 90 * diasFimSemana;
                            valor3 = 150 * diasSemana + 90 * diasFimSemana;
                        }

                        if (valor1 < valor2)
                        {
                            if (valor1 < valor3)
                            {
                                tipoCarro = "SUV�s";
                                melhorEmpresa = "NorthCar";
                                valor = valor1;
                            }
                            else
                            {
                                tipoCarro = "Esportivos";
                                melhorEmpresa = "WestCar";
                                valor = valor3;
                            }
                        }
                        else
                        {
                            if (valor2 < valor3)
                            {
                                tipoCarro = "Compactos";
                                melhorEmpresa = "SouthCar";
                                valor = valor2;
                            }
                            else
                            {
                                tipoCarro = "Esportivos";
                                melhorEmpresa = "WestCar";
                                valor = valor3;
                            }
                        }
                    }

                    string MenorData = menorData.Day.ToString() + "/" + menorData.Month.ToString() + "/" + menorData.Year.ToString() + "(" + menorData.DayOfWeek + ")";
                    string MaiorData = maiorData.Day.ToString() + "/" + maiorData.Month.ToString() + "/" + maiorData.Year.ToString() + "(" + maiorData.DayOfWeek + ")";

                    Cliente cliente = new Cliente(tipoCliente, numPassageiros, MenorData, MaiorData, tipoCarro, melhorEmpresa, valor);
                    TodosClientes.Add(cliente);
                    
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERRO: " + ex.Message.ToString();
            }

            return View(model: TodosClientes);
        }
        
        // GET: Clientes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Clientes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Clientes/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}